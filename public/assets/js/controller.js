$(window).ready(function(){
    var game_ended = false;
//crossword classes
$('#modal-close-button, .modal-play-button').on('click', function(){

    $("#message-modal-box-wrapper").fadeOut(200,function(){
        $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
        if(game_ended == true) {
            endGame();
        }
    });
});
$('#modal-final-close-button').on('click', function() {
    $("#message-modal-box-wrapper").fadeOut(200,function(){
        $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
        parent.$('body').trigger('game_ended');
    });
});
$("#start-message-modal-box").css("display","block");
$("#message-modal-box-wrapper").fadeIn(200);

var crossWordGenerator=function(wordsList)
{
    this.wordsList=wordsList;
};

crossWordGenerator.prototype=
{

    sortByLength:function()
    {
        var auxList2Sort= this.wordsList;
        var order=false;
        
        while(order==false)
        {
            var auxNotOrdered=false;
            for(i=0; i<auxList2Sort.length-1;i++)
            {
                if(auxList2Sort[i].length<auxList2Sort[i+1].length)
                {
                    var auxWord=auxList2Sort[i];
                    var auxWord2=auxList2Sort[i+1];
                    
                    auxList2Sort[i+1]=auxWord;
                    auxList2Sort[i]=auxWord2;
                    auxNotOrdered=true;
                }
                
            }
            if(auxNotOrdered==false)
            {
                order=true;
            }

        }
        return  auxList2Sort;
    },
    
    createMatrix:function(minSize)
    {
        var Rows=new Array(minSize+1);
        var h=0;
        for(i=0; i<Rows.length;i++)
        {
            var column= new Array(minSize+1);
            for(j=0;j<column.length;j++)
            {
                h++;
                column[j]="-";
            }
            Rows[i]=column;
        }
        
        return Rows;
    },
    
    placeWord:function(emptyMatrix, word)
    {
        ramdomDirection= (Math.floor(Math.random() * 9) % 2); //0: vertical, 1:horizontal
        
        var placed=false;
        while(placed==false)
        {
            ramdomTargetColumn=Math.floor(Math.random() * emptyMatrix.length);
            ramdomTargetRow=Math.floor(Math.random() * emptyMatrix.length);
            
            var emptySpaces=0;
            
            if(ramdomDirection==0)//vertical
            {
                emptySpaces=emptyMatrix.length -ramdomTargetRow;
                if(emptySpaces>=word.length)
                {
                    var canBePlaced=true;
                    for(k=0;k<word.length;k++)
                    {
                        var aux=emptyMatrix[ramdomTargetRow+k][ramdomTargetColumn];
                        if(aux=="-" ||aux==word[k])
                        {

                        }
                        else
                        {
                            canBePlaced=false;  
                        }
                    }

                    if(canBePlaced)
                    {
                        for(h=0;h<word.length;h++)
                        {
                            emptyMatrix[ramdomTargetRow+h][ramdomTargetColumn]=word[h];
                        }
                        placed=true;
                    }
                }
            }   
            else//horizontal    
            {
                var canBePlaced=true;
                for(k=0;k<word.length;k++)
                {
                    var aux=emptyMatrix[ramdomTargetRow][ramdomTargetColumn+k];
                    if(aux=="-" ||aux==word[k])
                    {

                    }
                    else
                    {
                        canBePlaced=false;  
                    }
                }

                if(canBePlaced)
                {
                    for(h=0;h<word.length;h++)
                    {
                        emptyMatrix[ramdomTargetRow][ramdomTargetColumn+h]=word[h];
                    }
                    placed=true;
                }
            }
        }
        
    },
    
    fillEmptyPlaces:function(emptyMatrix)
    {
        for(i=0;i<emptyMatrix.length;i++)
        {
            for(j=0;j<emptyMatrix.length;j++)
            {
                if(emptyMatrix[i][j]=="-")
                {
                    emptyMatrix[i][j]=randomString();
                }
            }
        }
        
    },
    
    drawBoard:function(board)
    {
        var q=0;
        var row="";
        for(i=0; i< board.length; i++)
        {   
            column=board[i];
            var col=""
            
            for(j=0; j<column.length;j++)
            {

                col+="<td><div collisionable='true' found='false' id='"+q+"'>"+column[j]+"</div></td>";
                q++;
            }   
            row+="<tr>"+col+"</tr>";
        }
        $("#board").html("<tbody>"+row+"</tbody>");
    }
    
}


function randomString() {

    var chars = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"];
    var randomstring=chars[Math.floor(Math.random() * chars.length)];
    return randomstring;
}   




function getCollionableDivsId()
{
    $('div').each(

        function()
        {
            var isCollionable=$(this).attr("collisionable");
            if(isCollionable=="true")
            {
                var currentDivId=$(this).attr("id");
                collisionableIds.push(currentDivId);
            }
        }
        
        );
    
    startGame=true;

}

var b = document.getElementById('game-board'),  
xbox = b.offsetWidth  / 1,  
ybox = b.offsetHeight / 1,   
bstyle = b.style;   


var position= new Array();
position["left"]=0;
position["top"]=0;

var startGame=false;


var isMouseDown=false;



var mouseMoveAction = function(event) {
    //event.preventDefault(); // the default behaviour is scrolling
    
    // var x=event.x;
    // var y=event.y;
    var x = event.pageX;
    var y = event.pageY;
    
    position.left=x;
    position.top=y;
    
    //console.log(event);
    if(isMouseDown==true)
    {
        checkCollition(position);
    }
}
var mouseDownAction = function(event) {

   // completeWord=[];
    event.preventDefault(); // the default behaviour is scrolling
    
    var x = event.pageX;
    var y = event.pageY;

    position.left=x;
    position.top=y;
    isMouseDown=true;
    
    //if(startGame==true)
    //{
        checkCollition(position);
    //}
    


}
var mouseUpAction = function(event) {

    isMouseDown=false;
    checkConstructedWord();

}

/* Attach events */
if (b.addEventListener) {
    b.addEventListener('mousemove', mouseMoveAction, false);
    b.addEventListener('mousedown', mouseDownAction, false);
    b.addEventListener('mouseup', mouseUpAction, false);
}
else {
    b.attachEvent('mousemove', mouseMoveAction);
    b.attachEvent('mousedown', mouseDownAction);
    b.attachEvent('mouseup', mouseUpAction);
}

function getOffset( el ) {
    var _x = 0;
    var _y = 0;
    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return { top: _y, left: _x };
}

var oldCollision="";
var newCollision="";


function checkCollition(touchPosition)
{   
    var auxTouchX=touchPosition.left;
    var auxTouchY=touchPosition.top;
    
    //$("#positionReporter").html("X position:"+auxTouchX+"<br/> Y position:"+auxTouchY);

    for(i=0; i<collisionableIds.length; i++)
    {
            //var auxColliderPos=$("#"+collisionableIds[i]).parent().position();
            var auxColliderPosX=getOffset( document.getElementById(collisionableIds[i]) ).left;//auxColliderPos.left;
            var auxColliderPosY=getOffset( document.getElementById(collisionableIds[i]) ).top;//auxColliderPos.top;
            
            //var x = getOffset( document.getElementById(collisionableIds[i]) ).left;
            //var y = getOffset( document.getElementById(collisionableIds[i]) ).top; 
            
            var collisionX=false;
            var collisionY=false;
            
            if(auxColliderPosX<=auxTouchX && auxColliderPosX+22>=auxTouchX)
            {
                var collisionX=true;    
            }
            
            if(auxColliderPosY<=auxTouchY && auxColliderPosY+22>=auxTouchY)
            {
                var collisionY=true;    
            }
            if(collisionY==true && collisionX==true)
            {
                $("#"+collisionableIds[i]).parent().css("color","#FFA000");
                
                newCollision=collisionableIds[i];
                
                //new collision reported
                if(oldCollision!=newCollision)  
                {
                    inCollision.push($("#"+collisionableIds[i]).attr("id"));
                    completeWord.push($("#"+collisionableIds[i]).html());
                    
                    auxCounter++;
                    oldCollision=newCollision;
                }           
                //console.log("Collision:" +collisionableIds[i]);
                //$("#status").html("Collision:" +collisionableIds[i]);
            }   
        }



    }

    var completeWord= new Array();
    var auxCounter=0;
    var inCollision=new Array();
    var wordsCompleted=0;

    function checkConstructedWord()
    {
        var word=""
        var found=false;

        for(i=0; i<completeWord.length;i++)
        {
            word+=completeWord[i];

        }

        for(j=0; j<words.length;j++)
        {
            if(word==words[j])
            {
                $('[index="'+j+'"].result').attr('result','correct');

                $("#title-word").html(word);
                $("#related-message").html(complementaryText[word]);

                var _class = '.word-selector.' + word.toLowerCase();

                $(_class).addClass('checked');


            //alert(complementaryText[word]);

            $("#message-modal-box").css("display","block");

            $("#message-modal-box-wrapper").fadeIn(200,function(){
               //  setTimeout(function(){
               //     $("#message-modal-box-wrapper").fadeOut(200,function(){$("#message-modal-box").css("display","none");});
               // },timerShow);
           });

            found=true;
        }
    }

    
    
    if(found)
    {
        //Search word in array
        var a = completedWordArr.indexOf(word);
        if(a==-1)
        {
            completedWordArr.push(word);
            wordsCompleted++;
            for(i=0; i<inCollision.length;i++)
            {
                $("#"+inCollision[i]).attr("found","true");
            }
            if(wordsCompleted==words.length)
            {
                game_ended = true;;
            }
        }


        
        
    }
    else
    {
        for(i=0; i<inCollision.length;i++)
        {
            if($("#"+inCollision[i]).attr("found")!="true")
            {
                $("#"+inCollision[i]).parent().css("color","");
            }
        }
    }

    inCollision=[];
    completeWord=[];

}



var completedWordArr=[];


//end of crossword classes
var timerShow=6000;

var complementaryText=
{
    Conocimiento:"Cuando trabajamos por procesos entre todos se construye y cada uno pone el conocimiento al servicio del cliente y consumidor.",
    Optimización: "Al trabajar por procesos logramos la optimización de los recursos de la organización  y evitamos duplicidad de acciones.",
    Proceso:"Cada proceso es diseñado para agregar valor por medio de la transformación de los insumos en una “salida” o producto específico.",
    Resultados:"Una organización que trabaja por procesos privilegia los resultados del Negocio sobre los resultados individuales o de las áreas.",
    Secuencia:"Un proceso es una secuencia de actividades interdependientes que utilizan insumos y recursos y se desarrollan de una manera programada y coordinada para lograr un objetivo común.",
    Estrategia:"El objetivo de trabajar por procesos es lograr que la estrategia del Negocio se haga realidad en el día a día de la Organización.",
    Cliente:"Cuando trabajamos por procesos todas las funciones (áreas) consideran en su cotidianidad al consumidor y cliente.",
    Consumidor:"Para lograr la estrategia (declaración estratégica y capacidades) es importante articular y conectar la actuación del Negocio de cara al consumidor y al cliente, lo cual implica revisar los procesos actuales, ver las oportunidades de cara a la estrategia y ajustar los procesos con un nuevo deber ser."
    
};


function endGame()
{
    setTimeout(function(){

        $("#message-final-box").css("display","block");

        $("#message-modal-box-wrapper").fadeIn(200,function(){
            // setTimeout(function(){
            //        $("#message-modal-box-wrapper").fadeOut(200,function(){$("#message-final-box").css("display","none");});
            // },timerShow);
        })

    },500);

    //alert("Finalizado");
}




var words; //contains all the words to create the puzzle
var collisionableIds= new Array();

init();


function init()
{

    words = ["Consumidor","Cliente","Estrategia","Secuencia","Resultados","Proceso","Optimización","Conocimiento"];
    
    getCollionableDivsId();

}

});

