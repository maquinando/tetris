var game_status = {
    started: false,
    ended: false,
    level: 0,
    lines: 0,
    paused: false,
    sound: true,
    speed: 10,
    gameOver: false
};
var d = new Date();
game_status.startTime = d.getTime();


var messages = [{
    message: 'El objetivo de <strong>trabajar por procesos</strong> es lograr que la estregia del negocio se haga realidad en la Organización.'
},{
    message: 'Cuando <strong>trabajamos por procesos</strong> tenemos mayor conciencia de las implicaciones que tienen nuestra labor en otras áreas.'
},{
    message: '<strong>Trabajar por procesos</strong> implica tener la capacidad de identificar, con la oportunidad adecuada, condiciones cambiantes del entorno y adaptarse a estas.' 
},{
    message: 'En el <strong>Trabajo por procesos</strong> los resultados de una persona o equipo se llaman \"salidas\" y se convierten en insumos o \"entradas\" para otras áreas.'
},{
    message: 'Tanto individuos como equipos deben estar conscientes de las implicaciones de su actuación en los diferentes <strong>procesos,</strong> para estar alineados en tiempos, entradas y salidas buscando sincronía y efectividad.'
},{
    message: '<strong>Trabajando por procesos</strong> logramos mayor claridad y agilidad en la toma de decisiones.'
},{
    message: 'Al <strong>trabajar por procesos</strong> se busca que los equipos queden convencidos de que lo que van a hacer tiene sentido. Entre todos se construye y cada uno pone el conocimiento al servicio del cliente y consumidor.'
}];

(function($) {
    var $game;
    var _audioVolume = 0.4;
    var audio = new Audio('assets/audio/Jingle.mp3');
    audio.addEventListener('load', function(event) {
        audio.volume = _audioVolume;
    });
    audio.volume = 0.2;
    var wasPaused = game_status.paused;
    var wasSound = game_status.sound;
    var oldLine = null;
    var oldLevel = null;
    var nextMessage = null;

    var _lineMsg = 0;
    var _nextMessageTimeout = 20 * 1000;
    var _nextMessageTimer = null;


    function gameUpdate() {
        if(!game_status.started) return;
        if(wasPaused && !game_status.paused) {
            $('#tetris-pause-button').removeClass('paused');
            $game.blockrain('resume');
            if(_nextMessageTimer != null) {
                clearTimeout(_nextMessageTimer);
            }
            _nextMessageTimer = setTimeout(showNextMessage, _nextMessageTimeout);
        }
        else if(!wasPaused && game_status.paused) {
            $('#tetris-pause-button').addClass('paused');
            $game.blockrain('pause');
            if(_nextMessageTimer != null) {
                clearTimeout(_nextMessageTimer);
            }
            _nextMessageTimer = null;
        }
        wasPaused = game_status.paused;
        if(wasSound && !game_status.sound) {
            $('#tetris-volume-button').addClass('muted');
            audio.pause();
        }
        else if(!wasSound && game_status.sound) {
            $('#tetris-volume-button').removeClass('muted');
            audio.volume = _audioVolume;
            audio.play();
        }
        wasSound = game_status.sound;

        // if(!game_status.started) return;
        // wasStarted = game_status.started;



        if(oldLine != game_status.lines) {
            var _lines = ("0" + game_status.lines).slice(-2);
            $('#current-line-wrapper').text(_lines);
        }
        oldLine = game_status.lines;
        // if(oldLevel != game_status.level) {
        //     var _level = ("0" + game_status.level).slice(-2);
        //     $('#current-level-wrapper').text(_level);
        // }
        // oldLevel = game_status.level;
    }

    function showNextMessage() {
        if(game_status.ended || game_status.paused) return;
        if( typeof messages[_lineMsg] == 'undefined') 
            return;
        showMessage('', messages[_lineMsg].message);
        _lineMsg ++;

        if(_lineMsg >= messages.length) {
            game_status.ended = true;
        } 
        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = null;

    }
    function showMessage(title, message) {
        if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible'))
            return;
        game_status.paused = true;
        $modalBox = $('#message-modal-box');
        if(title == '') {
            $modalBox.find('#title-word').hide();
        } else {
            $modalBox.find('#title-word').show().html(title);
        }
        if(message == '') {
            $modalBox.find('#related-message').hide();
        } else {
            $modalBox.find('#related-message').show().html(message);
        }
        
        $('#message-modal-box').css('display', 'inline-block');
        $('#message-modal-box-wrapper').fadeIn('slow');
    }
    function onLineEnded(lines, scoreIncrement, score){
        game_status.lines += lines;
        var mod = game_status.lines % 5;
        if(mod == 0) {
            game_status.level ++;
            game_status.speed ++;
            // $game.blockrain('speed', game_status.speed);
        }
        showNextMessage();
    }
    function onGameOver(score) {
        if($('#message-final-box').is(':visible') || $('#start-message-modal-box').is(':visible')) {
            $('#message-final-box').hide();
            $('#start-message-modal-box').hide();
        }

        // game_status.ended = true;
        showMessage("Intentalo de nuevo","Juega nuevamente y podrás descubrir la importancia de la <strong>gestión por procesos.</strong>");
        game_status.gameOver = true;
        
        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = null;
        
        // onGameEnded();
    }
    function onGameStart() {
        // showMessage("Alerta!!","Nuevo juego!");
        game_status.started = true;
        game_status.paused = false;
        var _shape = $(this).blockrain('nextShape');
        $('.current-form-wrapper').css('background-image', 'url("assets/img/shapes/'+_shape.blockType+'.png")');

        

    }
    function onGameRestart() {
        // showMessage("Alerta!!","Juego reiniciado.!");
        game_status.paused = false;
        var _shape = $(this).blockrain('nextShape');
        $('.current-form-wrapper').css('background-image', 'url("assets/img/shapes/'+_shape.blockType+'.png")');

        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = setTimeout(showNextMessage, _nextMessageTimeout);

    }
    function onGameEnded() {
        // showMessage('Alerta!!', 'Juego terminado!!');
        if($('#message-modal-box').is(':visible') || $('#start-message-modal-box').is(':visible')) {
            $('#message-modal-box').hide();
            $('#start-message-modal-box').hide();
        }
        
        $("#message-final-box").css("display","block");
        $("#message-modal-box-wrapper").fadeIn(200);
        setTimeout(function() {
            parent.$('body').trigger('game_ended');
        }, 3000);

        if(_nextMessageTimer != null) {
            clearTimeout(_nextMessageTimer);
        }
        _nextMessageTimer = null;
    }
    function onBlockPlaced(e) {
        // console.log(e);
        var _shape = $(this).blockrain('nextShape');
        $('.current-form-wrapper').css('background-image', 'url("assets/img/shapes/'+_shape.blockType+'.png")');
    }

    $(document).ready(function() {
        $game = $('#tetris-canvas');
        $('#modal-close-button, .modal-play-button').on('click', function(){
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
                if(game_status.ended == true) {
                    onGameEnded();
                } else if(game_status.gameOver) {
                    game_status.ended = false;
                    game_status.gameOver = false;
                    $game.blockrain('restart');
                } else {
                    game_status.paused = false;
                    if(_nextMessageTimer != null) {
                        clearTimeout(_nextMessageTimer);
                    }
                    _nextMessageTimer = setTimeout(showNextMessage, _nextMessageTimeout);
                }
            });
        });
        $('#modal-final-close-button').on('click', function() {
            $("#message-modal-box-wrapper").fadeOut(200,function(){
                $("#message-modal-box, #start-message-modal-box, #message-final-box").css("display","none");
                game_status.paused = false;
                if(_nextMessageTimer != null) {
                    clearTimeout(_nextMessageTimer);
                }
                _nextMessageTimer = setTimeout(showNextMessage, _nextMessageTimeout);
            });
        });
        

        $game.blockrain(
        {
            theme: 'nutresa',


            speed: game_status.speed,
            blockWidth: 12, // How many blocks wide the field is (The standard is 10 blocks)
            autoBlockWidth: false, // The blockWidth is dinamically calculated based on the autoBlockSize. Disabled blockWidth. Useful for responsive backgrounds
            autoBlockSize: 12,
            showFieldOnStart: true, // Show a bunch of random blocks on the start screen (it looks nice)

            // Copy
            playText: 'Juguemos al Tetris.',
            playButtonText: 'Jugar',
            gameOverText: 'Juego Terminado',
            restartButtonText: 'Volver a Jugar',
            scoreText: 'Marcador',


            // Basic Callbacks
            onStart: onGameStart,
            onRestart: onGameRestart,
            onGameOver: onGameOver,

            // When a line is made. Returns the number of lines, score assigned and total score
            onLine: onLineEnded,

            onPlaced: onBlockPlaced,
        });
        /* Start automatically */
        $("#start-message-modal-box").css("display","block");
        $("#message-modal-box-wrapper").fadeIn(200);
        $game.blockrain('start');
        game_status.paused = true;

        $('#tetris-pause-button').on('click', function() {
            if(game_status.paused) {
                game_status.paused = false;
            } else {
                showMessage('', '<strong>Juego en pausa!!</strong>');
            }
        });
        $('#tetris-volume-button').on('click', function() {
            game_status.sound = !game_status.sound;
        });
        $(window).blur(function(){
          //your code here
          showMessage('', '<strong>Juego en pausa!!</strong>');
      });

        audio.volume = _audioVolume;
        audio.play();
        setInterval(gameUpdate, 60);

        
    });
})(jQuery);